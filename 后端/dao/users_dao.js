
global.globalkey = '123456'; //秘钥
const jwtUtils = require('../utils/jwtUtils');
const redisUtils = require('../utils/redisUtils');

module.exports = class users_dao extends require('../model/users_mod'){

    /**
     *  登录接口
     *
     * */
    static async Login(req, resp){
        let body = req.body;
        let loginData = await this.LoginUser(body.username,body.password,body.type);
        // 如果获取了用户信息则登录
        if(loginData.length != 0){
            let jwt_token = jwtUtils.sign({
                id: loginData[0].id,
                username: loginData[0].username,
                head: loginData[0].head,
                type: loginData[0].type
            },global.globalkey,3600) //3600 超时时间
            // 这个 .send 里必须添加｛｝，没有括号返回不回去
            console.log("loginData",loginData)
            resp.status(200).send({jwt_token, loginData})
        }else{
            resp.status(500).send('用户名或账号输入错误')
        }
    }

    /**
     *  根据 token 解析成用户信息
     *
     * */
    static async getUserDataByToken(req,resp){
        // 因为是异步的，所有要加 await async
        let result = await jwtUtils.verifysync(req.query.token, global.globalkey);
        resp.send(result)
    }

    /**
     *  根据用户类型进行用户信息获取(分页获取总数量与数据)
     * */
    static async getUsersByTypePage(req, resp){
        /**
         * 起名的时候用的是 getUsersByTypePageMod，而不是 getUsersByTypePage 时候因为 getUsersByTypePage 是
         *  父类继承过来的，如果一个名字的话会被重写
         */
        console.log("req",req.query)
        let query = req.query;
        let data = await this.getUsersByTypePageMod(query.type, query.currPage, query.pageNum);
        let total = await this.getUsersByTypePageTotal(query.type);
        resp.send({ data, total:total[0]});
    }

    /**
     *  用户删除（同时清空改用户阅读记录）
     * */
    static async delUserdata(req, resp){
        let results = await this.delUserdataMod(req.query.u_id);
        results += await this.delRead(req.query.u_id);
        resp.send(results);
    }

    /**
     *  用户信息修改
     * */
    static async upUserdata(req, resp){
        let body = req.query;
        let u_id = body.u_id;
        let username = body.username;
        let sex = body.sex;
        let address = body.address;
        let type = body.type;
        let results = await this.upUserdataMod(u_id,username,sex,address,type);
        resp.send(results);
    }


    /**
     *  将redis的xlsx数据写入数据库
     * 
     */
     /**
     * 将redis的xlsx数据写入数据库
     * @param req
     * @param resp
     * @returns {Promise<void>}
     */
      static async setXlsxData(req,resp){
        // console.log("点击导入")
        let xlsxData=await redisUtils.get("xlsxData")
        let AllUsers=await this.getAllUserX();
        console.log("12121212121212",AllUsers)
        if (xlsxData=='err') {
            resp.send("导入失败,不是标准的文件格式")
            return
        }
        xlsxData=JSON.parse(xlsxData)[0].data
        let inXlsxArr=[];
        let infalg=true;
        // console.log(xlsxData[0].length)
        if (xlsxData[0].length!=8) resp.send("导入的表格数据格式错误")

        for (let i=1;i<xlsxData.length;i++){
            if (xlsxData[i][0]!=0){
                let falg=true;
                let xlsxObj={};
                if (xlsxData[i][0]==null || xlsxData[i][1]==null || xlsxData[i][2]==null || xlsxData[i][4]==null || xlsxData[i][5]==null || xlsxData[i][6]==null || xlsxData[i][7]==null) {
                    infalg=false
                }
                xlsxObj.id=xlsxData[i][0]
                xlsxObj.username=xlsxData[i][1]
                xlsxObj.password=xlsxData[i][2]
                xlsxObj.head=xlsxData[i][3]||''
                xlsxObj.address=xlsxData[i][4]
                xlsxObj.sex=xlsxData[i][5]
                xlsxObj.classes=xlsxData[i][6]
                xlsxObj.type=xlsxData[i][7]
                for (let j=0;j<AllUsers.length;j++){
                    if (xlsxObj.id==AllUsers[j].id) falg=false
                }
                if (falg) inXlsxArr.push(xlsxObj)
            }
        }
        if (infalg){
            if (inXlsxArr.length!=0) this.inXlsxData(inXlsxArr)
            resp.send("导入数据成功")
        }else resp.status(500).send("导入文件中的数据部分格式错误,导入失败")

    }
    // static async setXlsxData(req,resp){
    //     let xlsxData = await redisUtils.get('xlsxData');
    //     let AllUsers = await this.getAllUserX();
    //     console.log("AllUsers",AllUsers);
    //     if(xlsxData=='err'){
    //         return resp.send("导入失败，不是标准的文件格式");
    //     }
    //     console.log("xxxxxxxxxxxxxxxxxxx");
        

    //     xlsxData = JSON.parse(xlsxData)[0].data;
    //     console.log("xlsxData",xlsxData);
    //     let inXlsxArr = [];
    //     let inflag = true;
    //     if(xlsxData[0].length !==8){
    //         // xlsxData[0] 为表头
    //         return resp.send('导入格式失败,表头为8个');
    //     }
    //     for(let i in xlsxData){
    //         console.log("i",i)
    //         if(i>1){
    //             console.log("xlsxData[i][0]",xlsxData[i][0])
    //             console.log("123123")
    //             // if(xlsxData[i][0] != 0){ 
    //                 let flag = true;
    //                 let xlsxObj = {};
    //                 for(let key in xlsxData[i][0]){
    //                     console.log("xlsxData[i][0][key]",xlsxData[i][0][key])
    //                     if(xlsxData[i][0][key] == null){
    //                         inflag = false;
    //                     }
    //                     xlsxObj.id = xlsxData[i][0];
    //                     xlsxObj.username = xlsxData[i][1];
    //                     xlsxObj.password = xlsxData[i][2];
    //                     xlsxObj.head = xlsxData[i][3] || '';
    //                     xlsxObj.address = xlsxData[i][4];
    //                     xlsxObj.sex = xlsxData[i][5];
    //                     xlsxObj.classes = xlsxData[i][6];
    //                     xlsxObj.type = xlsxData[i][7];
    //                     console.log("xlsxObj",xlsxObj)
    //                     // 验证是否是已经导入的数据 demo用的是id，正常应该是身份证
    //                     for(let j=0;j<AllUsers.length;j++){
    //                         if(xlsxObj.id==AllUsers.id){
    //                             flag = false;
    //                         }
    //                     } 
    //                     if(flag){ 
    //                         inXlsxArr.push(xlsxObj);
    //                     }
    //                 } 
    //             // }
    //         }
    //     }  
    //     console.log("inflag",inflag)
    //     console.log("inXlsxArr.length",inXlsxArr.length)
    //     if(inflag){
    //         if(inXlsxArr.length !=0){
    //             this.inXlsxData(inXlsxArr);
    //             resp.send('导入数据成功');
    //         }
    //     }else{
    //         resp.status(500).send('导入文件中的数据部分格式错误失败，导入失败')
    //     }

    // }

}
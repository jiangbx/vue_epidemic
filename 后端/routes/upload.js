var express = require('express');
var router = express.Router();

let fs = require('fs');
let formidable = require('formidable');
let xlsx = require('node-xlsx');
const redisUtils = require('../utils/redisUtils');

router.post('/upload',function(req, res){
    console.log("###### post/upload #####");
    let fileTpyeError = false;
    // 上传的路径
    let target_path = `./public/upload`;
    let form = new formidable.IncomingForm();
    form.encoding = 'utf-8';
    form.keepExtends = true;
    form.maxFieldsSize = 10 * 1024 * 1024;
    form.uploadDir = target_path; //文件上传 临时文件存放路径

    let file = [];  // 文件
    let fields = []; // 领域
    form.on('field', function(filed, value){
        fileds.push([filed, value]);
    });

    //文件上传开始
    form.on('file', function(field, file){
        console.log("file:" + file);
        console.log("fileName:" + file.name);
        let filext = file.name.substring(file.name.lastIndexOf("."), file.name.length);
        if(filext !== '.xlsx'){
            /**
             * 设置键值
             * @param key 键
             * @param value 值
             * @param expire 过期时间（单位：秒，可为空，为空则不过期）
             * @param dbNum 库号
             */
            redisUtils.set('xlsxData','err',3600)
            fileTpyeError = true;
        }else{
            fileTpyeError = false;
            return
        }
        files.push([field,file])
    });

    //文件上传结束
    form.on('end',async function () {
        //遍历上传的文件
        let fileName = '';
        let obj = '';
        let exflag = true;
        //　fs.existsSync　以同步的方法检测目录是否存在。  如果目录存在 返回 true ，如果目录不存在 返回false
        let folder_exists = await fs.existsSync(target_path);

        if(folder_exists){
            // fs.readdirSync 返回一个包含“指定目录下所有文件名称”的数组对象。
            let dirList = await fs.readdirSync(target_path);
            console.log("dirList",dirList)
            dirList.forEach(item=>{
                if(!fs.statSync(target_path + `\\` + item).isDirectory()){
                    fileName = target_path + `\\` + item;
                    if(!fileTpyeError){
                        obj = xlsx.parse(fileName);
                        // 重复写入会覆盖，等下有时间加个时间戳唯一不重复就好
                        redisUtils.set('xlsxData',JSON.stringify(obj),3600);
                        res.send({'rtnCode':'0', 'rtnInfo':'成功导入数据', 'data':obj })
                    }else{
                        //解析失败
                        res.send({'rtnCode':'1', 'rtnInfo':'导入数据失败'})
                        exflag = false;
                    }
                    //删除找到的文件（现在未加时间戳）
                    fs.unlinkSync(fileName);
                }else{
                    return res.send({"rtnCode":"1", "rtnInfo":"系统错误"});
                }
            })
        }
    })

    form.on('error', function (err) {
        res.send({'rtnCode':'1', 'rtnInfo':'上传出错'})
    });

    form.on('aborted', function (err) {
        res.send({'rtnCode':'1', 'rtnInfo':'放弃上传'})
    });
    form.parse(req)



})



module.exports = router;
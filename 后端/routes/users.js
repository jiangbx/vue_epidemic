var express = require('express');
var router = express.Router();
// console.log("router",router)
const users = require('../dao/users_dao');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('users进入路由根目录22');
});

/**
 *  登录
 * */
//没有. 不是.login 妈的之前些的不是 /login 浪费了我一个小时去找为啥不好使
router.post('/login', function (req,res,next) {
    //直接转发出去就行了
    users.Login(req,res)
});

/**
 *  解析token
 * */
router.get('/getUserDataByToken', function (req,res) {
    //路由接口主要用来转发
    users.getUserDataByToken(req,res)

})

/**
 * 根据用户类型进行用户信息获取(分页获取总数量与数据)学生
 * */
router.get('/getUsersByTypePage', function (req,res) {
    //路由接口主要用来转发
    users.getUsersByTypePage(req,res)

})

/**
 *  删除数据
 * */
router.get('/delUserdata', function (req, res) {
    if(req.query.u_id == 0) {
        res.send("不能刪除管理員");
    }else{
        users.delUserdata(req,res);
    }

})

/**
 *  用户信息修改
 * */
router.post('/upUserdata', function (req, res) {
    users.upUserdata(req,res);
})

/**
 *  用户信息 xlsx 导入
 * */
router.post('/setXlsxData', function (req, res) {
    users.setXlsxData(req,res);
})


module.exports = router;

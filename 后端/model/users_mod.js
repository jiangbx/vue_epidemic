const tools = require("../utils/tools");

module.exports = class users_mod extends require('./model'){

    /**
     *  登录数据库
     * */
    static LoginUser(username, password, type){
        type = Number(type);
        return new Promise((resolve, reject)=>{
            let sql = "select * from user where binary username='"+username+"' and password='"+password+"' and type= " + type;
            // let sql = "select * from sp_manager where binary mg_name='"+username+"' and mg_pwd='"+password+"' ";
            console.log(sql)
            this.query(sql).then(result=>{
                console.log("成功")
                resolve(result)
            }).catch(err=>{
                console.log("登录失败")
                reject('登录失败')
            })
        })
    }


    /**
     *  根据用户类型进行用户信息获取(分页获取总数量)
     * */
    static getUsersByTypePageMod(type, currPage, pageNum){
        pageNum = Number(pageNum);
        currPage = Number(currPage);
        currPage = Number(currPage*pageNum);
        return new Promise((resolve, reject)=>{
            let sql = `select * from user where type =${type} order by modifytime desc LIMIT ${currPage},${pageNum}`;
            console.log("sql",sql);
            this.query(sql, this.formatParams(currPage, pageNum)).then(result=>{
                resolve(result);
            }).catch(err=>{
                reject(err);
            })
        })
    }

    /**
     *  根据用户类型进行用户信息获取(分页获取总数量)
     * */
    static getUsersByTypePageTotal(type){
        return new Promise((resolve, reject)=>{
            let sql = `select count(1) from user where type = ${type}`;
            console.log("sql",sql);
            this.query(sql).then(result=>{
                resolve(result);
            }).catch(err=>{
                reject(err);
            })
        })
    }

    /**
     *  用戶删除（同时清空改用户阅读记录）
     * */
    static async delUserdataMod(id){
        return new Promise((resolve, reject) => {
            let sql = `delete user from user where id=${id}`;
            console.log("sql",sql);
            this.query(sql).then(result=>{
                resolve("删除用户表用户成功");
            }).catch(err=>{
                reject("删除用户表用户失败");
            })
        })
    }

    /**
     *  删除阅读表用户
     * */
    static delRead(id){
        return new Promise((resolve, reject)=>{
            let sql = `delete from \`read\` where u_id=${id}`;
            console.log(sql);
            this.query(sql).then(result=>{
                resolve("删除阅读表用户成功");
            }).catch(err=>{
                reject("删除阅读表用户失败");
            })
        })
    }

    /**
     *  管理员进行用户修改
     * */
    static upUserdataMod(u_id,username,sex,address,type){
        let currTime = tools.getDate19();
        return new Promise((resolve, reject) => {
            let sql = `update \`user\` set username='${username}', sex='${sex}', `+
                      `address='${address}', type='${type}', modifytime='${currTime}' `+
                      `where id='${u_id}'`;
            console.log(sql);
            this.query(sql).then(result=>{
                resolve("更新成功");
            }).catch(err=>{
                reject("更新失败")
            })
        })
    }

    /***
     *  将redis数据未重复的进行插入
     *  inXlsxArr
     */
    static inXlsxData(inXlsxArr){
        return new Promise((resolve,reject)=>{
            for(let i=0;i<inXlsxArr.length;i++){
                let sql = `insert into user(id,username,password,head,address,sex,classes,type)`+
                          ` values(${inXlsxArr[i].id}, '${inXlsxArr[i].username}', '${inXlsxArr[i].password}', '${inXlsxArr[i].head}',`+
                          ` '${inXlsxArr[i].address}', '${inXlsxArr[i].sex}', '${inXlsxArr[i].classes}', '${inXlsxArr[i].type}')`;
                console.log("inXlsxData",sql)
                this.query(sql).then(result=>{
                    resolve(result)
                }).catch(err=>{
                     reject(err)   
                })
            }
        })
    }

    /**
     *  获取所有用户信息
     * */
     static getAllUserX(){
        return new Promise((resolve, reject)=>{
            let sql = `select * from user `;
            console.log("sql",sql);
            this.query(sql).then(result=>{
                resolve(result);
            }).catch(err=>{
                reject(err);
            })
        })
    }

}
module.exports = class admin_mod extends require('./model'){

    /**
     *  分页搜索获取信息
     *     小到大排序（升序关键字 asc）
     *     大到小排序（降序关键字 desc）
     *     数字去重 （ distinct） select distinct 列名 from 默认升序
     * */

    static getUsersByTypeAndCharMod(type, inputText, CharType, currPage, pageNum){
        pageNum = Number(pageNum);
        currPage = Number(currPage);
        currPage = Number(currPage*pageNum);
        return new Promise((resolve, reject)=>{
            let sql = `select * from user where ${CharType} like "%${inputText}%"`+
                      ` and type = ${type} order by modifytime desc limit ${currPage},${pageNum}`;
            console.log("模糊查询sql",sql)
            this.query(sql, this.formatParams(currPage,pageNum)).then(result=>{
                resolve(result);
            }).catch(err=>{
                reject(err);
            })
        })
    }

    /**
     *
     *
     * */
    static getUsersByTypeCharTotal(type, inputText, CharType){
        return new Promise(((resolve, reject) => {
            let sql = `select count(1) as count from user where ${CharType} like "%${inputText}%" and type=${type}`;
            console.log("sql",sql)
            this.query(sql).then(result=>{
                resolve(result);
            }).catch(err=>{
                reject(err);
            })
        }))
    }

}